<?php
class Instructor extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor un instructor en mysql
  function insertar($datos){

  return $this->db->insert("instructor",$datos);
  }
  //FUNCION PARA CONSULTAR Instructores
  function obtenerTodos(){
    $listadoInstructores=$this->db->get("instructor");
    if($listadoInstructores->num_rows()>0){//SI HAY DATOOOOOS
      return $listadoInstructores->result();
    }else {
      return false;
    }
  }
  //BORRAR Instructores
  function borrar($id_ins){
    //DELTE FROM INSTRUCTOR WHERE id_ins
    $this->db->where("id_ins",$id_ins);
    if ($this->db->delete("instructor")) {
      return true;
    } else {
      return false;
    }

  }
} //CIERRE DE LA CLASE
 ?>
