<h1>Listado de Estudiantes</h1>
<?php if ($estudiantes): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE ESTUDIANTE</th>
        <th>EDAD DEL ESTUDIANTE</th>
        <th>CICLO DEL ESTUDIANTE</th>
        <th>MATERIA DEL ESTUDIANTE</th>
        <th>CEDULA DEL ESTUDIANTE</th>
        <th>CELULAR DEL ESTUDIANTE</th>
        <th>ACIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($estudiantes as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_estudiante; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nom_estudiante; ?>
          </td>
          <td>
            <?php echo $filaTemporal->edad_estudiante; ?>
          </td>
          <td>
            <?php echo $filaTemporal->ciclo_estudiante; ?>
          </td>
          <td>
            <?php echo $filaTemporal->materia_estudiante; ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_estudiante; ?>
          </td>
          <td>
            <?php echo $filaTemporal->numero_estudiante; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Estudiante">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/estudiantes/eliminar/<?php echo $filaTemporal->id_estudiante; ?>" title="Eliminar Instructor" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
