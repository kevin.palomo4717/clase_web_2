<h1>NUEVO ALUMNO</h1>
<form class="" action="<?php echo site_url(); ?>/estudiantes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre Estudiante</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del estudiante"
          class="form-control"
          name="nom_estudiante" value=""
          id="nom_estudiante">

      </div>
      <div class="col-md-4">
          <label for="">Edad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la edad del estudiante"
          class="form-control"
          name="edad_estudiante" value=""
          id="edad_estudiante">
      </div>
      <div class="col-md-4">
        <label for="">ciclo</label>
        <br>
        <input type="number"
        placeholder="Ingrese el ciclo del estudiante"
        class="form-control"
        name="ciclo_estudiante" value=""
        id="ciclo_estudiante">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Materia de estudiante</label>
          <br>
          <input type="text"
          placeholder="Ingrese la materia del estudiante"
          class="form-control"
          name="materia_estudiante" value=""
          id="materia_estudiante">

      </div>
      <div class="col-md-4">
          <label for="">CEDULA:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cedula del estudiante"
          class="form-control"
          name="cedula_estudiante" value=""
          id="cedula_estudiante">
      </div>
      <div class="col-md-4">
        <label for="">CELULAR</label>
        <br>
        <input type="number"
        placeholder="Ingrese el celular del estudiante"
        class="form-control"
        name="numero_estudiante" value=""
        id="numero_estudiante">
      </div>
    </div>
      <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/estudiantes/indexA"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
