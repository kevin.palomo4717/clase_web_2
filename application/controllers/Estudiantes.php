<?php

    class Estudiantes extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Estudiante');

        }



        //Funcion que renderiza la vista index

        public function indexA(){

            $data['estudiantes']=$this->Estudiante->obtenerTodos();
            $this->load->view('header');
            $this->load->view('estudiantes/indexA',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoe(){
            $this->load->view('header');
            $this->load->view('estudiantes/nuevoe');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosEstudiante=array("nom_estudiante"=>$this->input->post('nom_estudiante'),"edad_estudiante"=>$this->input->post('edad_estudiante'),
          "ciclo_estudiante"=>$this->input->post('ciclo_estudiante'),"materia_estudiante"=>$this->input->post('materia_estudiante'),"cedula_estudiante"=>$this->input->post('cedula_estudiante'),"numero_estudiante"=>$this->input->post('numero_estudiante')
        );
        if($this->Estudiante->insertar($datosNuevosEstudiante)){
          redirect('estudiantes/indexA');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_estudiante){
       if ($this->Estudiante->borrar($id_estudiante)){
         redirect('estudiantes/indexA');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
